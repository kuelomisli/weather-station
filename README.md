Arduino
---
This is an Arduino project: a weather station with a pressure and temperature
sensor (the Bosch BMP280), an LCD screen, (and a humidity sensor, which is planned.)

I'm using the azdelivery BMP280, with the Adafruit Library for their version of
the BMP280 though. The Adafruit library is better, but you need the change the
I2C address in their header to 0x76 for the azdelivery board to work.


