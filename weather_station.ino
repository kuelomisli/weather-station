//    Copyright (C) 2018 kuelomisli
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <Wire.h>
#include "Adafruit_HTU21DF.h"
#include <EEPROM.h>
#include <LiquidCrystal_I2C.h>
#include <Adafruit_BMP280.h>

#define BUTTON0           3
#define BUTTON1           4
#define BUTTON2           5
#define BUTTON3           6

// Need to set I2C Address in Adafruit_BMP280.h to 0x76 for azdelivery version
Adafruit_BMP280 bmp; // I2C

// Set the LCD address to 0x27 for a 16 chars and 2 line display
LiquidCrystal_I2C lcd(0x27, 16, 2);

// takes a measurement every interval, does that number_of_samples times before averaging all the measuremnts.
// only stores the averaged measurement.
double measurement_interval_in_minutes = 5;
double last_measurement = 0;
double number_of_samples = 11;

// for stations on fixed altitudes, to normalize to sea level; around 1200Pa per 100m
double altitude_compensation = 0.0;

int fast_refresh_time = 100;
int slow_refresh_time = 5000;
int interval = slow_refresh_time; // refresh interval of data on screen
String refresh = "SlR";

unsigned long previous_time, current_time = 0;
double current_minute = 0;
unsigned long hour = 0;
unsigned long second = 0;
int sample_counter = 0;
int display_backlight_state = 1;
int display_state = 1;
int realtime_mode = 0;

double T, P, H = 0;

double humidity_min = 0;
double humidity_max = 0;

double temperature_min = 0;
double temperature_max = 0;

double pressure_acc = 0;
double averaged_pressure = 101300.0;
//double history[24] = {101300.0};
double history[24] = {100200.0,100300.0,100400.0,100500.0,100600.0,100700.0,100800.0,100900.0,101000.0,101100.0
,101200.0,101300.0,101400.0,101500.0,101600.0,101700.0,101800.0,101900.0,102000.0,102100.0,102200.0,102300.0
,102400.0,102500.0};
//double history[12] = {100200.0,100300.0,100400.0,100500.0,100600.0,100700.0,100800.0,100900.0,101000.0,101100.0
//,101200.0,101300.0};
int history_size = sizeof(history) / sizeof(history[0]);
int history_showing = 0;
int history_show_duration = 7000;
int screen_duration = 2000;
int no_of_screens = 4;
int part = 0;

unsigned long debounce_interval = 250;
unsigned long last_debounce_time = 0;

Adafruit_HTU21DF htu = Adafruit_HTU21DF();

void change_backlight() {
  if (display_backlight_state == 0) {
    lcd.backlight();
    display_backlight_state = 1;
  }
  else if (display_backlight_state == 1) {
    lcd.noBacklight();
    display_backlight_state = 0;
  }
}

void switch_display() {
// turns on/off display and restores backlight state
  if (display_state == 0) {
    lcd.display();
    display_state = 1;
  if (display_backlight_state == 1 ) {
      lcd.backlight();
    }
    else {
      lcd.noBacklight();
    }
  }
   else {
    lcd.noBacklight();
    lcd.noDisplay();
    display_state = 0;
  }
}

void change_realtime_mode() {
  if (realtime_mode == 0) {
    interval = fast_refresh_time;
    realtime_mode = 1;
    refresh = "RT";
  }
  else {
    interval = slow_refresh_time;
    realtime_mode = 0;
    refresh = "";
  }
}

void every_hour() {
// update history
  for (int i = 1; i <= history_size; i++) {
    history[i - 1] = history[i];
  }
  history[history_size] = averaged_pressure;

// check max and min H&T
  if (H > humidity_max) humidity_max = H;
  if (H < humidity_min) humidity_min = H;
  if (T > temperature_max) temperature_max = T;
  if (T < temperature_min) temperature_min = T;

// reset every eight hours
// this is stupid, make it flow into the next cycle
  if (not (hour%8)) {
  humidity_max = H;
  humidity_min = H;
  temperature_max = T;
  temperature_min = T;
  }
}

int show_part(int this_part) {
// show part of the history full screen
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(history_size-this_part);
    lcd.print("h: ");
    if (abs(history[this_part] - averaged_pressure) < 1000.0) lcd.print(" ");
    if ((history_size-this_part) < 10 ) lcd.print(" ");
    for (int i = this_part; (i < this_part+2) and (i < history_size); i++) {
        double delta = history[i] - averaged_pressure;
        if (delta >= 0) lcd.print("+");
        lcd.print(String(delta / 100.0, 1)); lcd.print(" ");
        if (abs(delta) < 1000.0) lcd.print(" ");
    }
    lcd.setCursor(0, 1);
    for (int i = this_part+2; (i < this_part+5) and (i < history_size); i++) {
        double delta = history[i] - averaged_pressure;
        if ((delta >= 0)) lcd.print("+");
        lcd.print(String(delta / 100.0, 1)); lcd.print(" ");
        if (abs(delta) < 1000.0) lcd.print(" ");
    }
    delay(screen_duration);
    lcd.clear();
    return this_part+5;
}




//int show_part(int this_part) {
//// this mode cycles through all values on the lower part of the screen;
//// don't like it much, is slow and not very clear.
//// also, there's an out of bonds error here
//    lcd.clear();
//    for (int i = this_part; i < this_part+3; i++) {
//        lcd.clear();
//        lcd.setCursor(0, 0);
//        for (int j = i; j < i+3; j++) {
//            lcd.print(history_size-j); lcd.print("h   ");
//        }
//        lcd.setCursor(0, 1);
//        for (int j = i; j < i+3; j++) {
//            double delta = history[j] - averaged_pressure;
//            if ((delta >= 0)) lcd.print("+");
//            lcd.print(String(delta / 100.0, 1)); lcd.print(" ");
//            if (abs(delta) < 1000.0) lcd.print(" ");
//            }
//        delay(screen_duration);
//    }
//    return this_part+6;
//}
//
void show_history() {
  history_showing = 1;
  lcd.clear();
  while (part < history_size-1) {
  part = show_part(part);
  }
  part = 0;
  history_showing = 0;
  lcd.clear();
}

void show_extremes() {
    history_showing = 1;
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("min "); lcd.print(String(temperature_min,1)); lcd.print("C ");
    lcd.print(String(humidity_min,1)); lcd.print("%");
    lcd.setCursor(0,1); 
    lcd.print("max "); lcd.print(String(temperature_max,1)); lcd.print("C ");
    lcd.print(String(humidity_max,1)); lcd.print("%");
    delay(history_show_duration);
    lcd.clear();
    history_showing = 0;
    }

int read_button_state(int button, void callback())
{
  // Assuming all inputs are pulled up and buttons switch to low.
  int reading = digitalRead(button);

  if (reading == LOW) {
    if  ((millis() - last_debounce_time ) > debounce_interval) {
      last_debounce_time = millis();
      callback();
    }
  }
}


void print_current_data()
{
  // T in Celcius, P in Pascal, 10^5 Pa = 1 bar
  T = bmp.readTemperature();
  P = bmp.readPressure() + altitude_compensation;
  H = htu.readHumidity();

  lcd.setCursor(0, 0);
  lcd.print(String(T,1)); lcd.print(" C "); lcd.print(String(H,1)); lcd.print("% "); lcd.print(hour%8); //lcd.print(refresh);
  lcd.setCursor(0, 1);
  lcd.print(String(P/100.0,1)); lcd.print(" hPa "); lcd.print("h: "); lcd.print(hour);
  previous_time = millis();
}

void setup()
{
  pinMode(BUTTON0, INPUT_PULLUP);
  pinMode(BUTTON1, INPUT_PULLUP);
  pinMode(BUTTON2, INPUT_PULLUP);
  pinMode(BUTTON3, INPUT_PULLUP);

  // initialize the LCD
  lcd.begin();
  lcd.clear();
  lcd.backlight();
  lcd.setCursor(0, 0);
  lcd.print("A Pretty Precise");
  lcd.setCursor(0, 1);
  lcd.print("Weather Station");
  //  lcd.noBacklight();

  if (!htu.begin()) {
    lcd.setCursor(0, 0);
    lcd.print("Err: HTU21D");
  }
  
  if (!bmp.begin())
  {
    lcd.clear();
    lcd.setCursor(0,0); lcd.print(F("Error: BMP280"));
    lcd.setCursor(0,1); lcd.print(F("Init Failure"));
  }
    else {
      averaged_pressure = bmp.readPressure()+altitude_compensation;
  //for (int i = 0; i <= history_size; i++) {
  //  history[i] = averaged_pressure;
  //}
    }

    delay(50);
    T = bmp.readTemperature();
    delay(50);
    H = htu.readHumidity();
    delay(50);
    every_hour();

    lcd.clear();
}

void loop()
{

  current_time = millis();
  current_minute = (double) (millis() / 1000.0 / 60.0);
  /* counts hours since the program started running */
  if (current_time > (hour + 1) *3600* 1000) {
    hour++;
    every_hour();
  }
  // if (current_time > second*1000)  second++;

  read_button_state(BUTTON0, show_history);
  read_button_state(BUTTON1, change_backlight);
  read_button_state(BUTTON2, switch_display);
  read_button_state(BUTTON3, show_extremes);
  //read_button_state(BUTTON3, change_realtime_mode);

  if ((current_minute - last_measurement ) > measurement_interval_in_minutes ) {
    pressure_acc += bmp.readPressure()+altitude_compensation;
    sample_counter++;
    if (sample_counter >= number_of_samples) {
      averaged_pressure = pressure_acc / number_of_samples;
      pressure_acc = 0;
      sample_counter = 0;
    }
    last_measurement = current_minute;
  }

  if (((current_time - previous_time) > interval) & not history_showing)
  {
    print_current_data();
  }
}
